<?php
$gioitinh = array(0 => "Nam", 1 => "Nữ");
$phankhoa = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .box {
            width: 600px;
            height: 300px;
            display: block;
            border: 2px solid cadetblue;
            margin: auto;
            padding: auto;
            margin-top: 100px;
        }

        .item {
            display: flex;
            align-items: center;
            margin-left: 50px;

        }

        .items {

            border: 1px solid #007bc7;
            background-color: #4ba3ff;
            padding: 10px;
            margin-right: 20px;

        }

        .name {
            width: 250px;
            height: 30px;

        }

        .ipgioitinh {
            margin-left: 20px;
        }

        .phankhoa {
            height: 30px;
        }

        .btn {
            padding: 15px;
            border-radius: 10px;
            background-color: #39bc64;
            margin-left: 45%;
        }
    </style>
</head>

<body>
    <form>
        <div class="box">
            <div class="item" style="margin-top: 10px;">
                <p class="items">Họ và tên</p>
                <input type="text" class="name">

            </div>
            <div class="item">
                <p class="items">Giới tính</p>
                <?php foreach ($gioitinh as $key => $value) : ?>
                    <label for=<?= $key ?>>
                        <?= $value; ?>
                    </label>
                    <input required id=<?= $key ?> type="radio" name="giotinh" value=<?= $key ?> class="ipgioitinh">
                <?php endforeach; ?>
            </div>
            <div class="item">
                <p class="items">
                    <label for="phankhoa">
                        Phân khoa
                    </label>
                </p>
                <select required name="phankhoa" id="phankhoa" class="phankhoa">
                    <option disabled selected value></option>
                    <?php foreach ($phankhoa as $key => $value) : ?>
                        <option value="<?= $key ?>"><?= $value ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <input type="submit" class="btn" value="Đăng Kí">
        </div>
    </form>
</body>

</html>